package com.gabriel.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.gabriel.model.Persona;
import com.gabriel.service.IPersonaService;

@RestController
@RequestMapping("/persona")
public class PersonaController {
	@Autowired
	IPersonaService service;
	

	@PostMapping(value="/registrar/",consumes=MediaType.APPLICATION_JSON_VALUE , produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Persona> registrarPersona(@RequestBody Persona personaEntrante){
		Persona persona = new Persona();
		try {
			persona = service.registrar(personaEntrante);
		}catch(Exception e) {
			return new ResponseEntity<Persona>(persona,HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<Persona>(persona,HttpStatus.OK);
	}
	
	@PutMapping(value = "/modificar/",consumes=MediaType.APPLICATION_JSON_VALUE , produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Integer> modificarPersona(@RequestBody Persona personaEntrante){
		
		int resultado = 0;
		try {
			 service.modificar(personaEntrante);
			 resultado = 1;
		}catch(Exception e) {
			return new ResponseEntity<Integer>(resultado,HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<Integer>(resultado,HttpStatus.OK);
	}
	
	@GetMapping(value = "/listar/{id}",produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Persona> listarPersonaId(@PathVariable("id") Integer idPersona){
		Persona persona = new Persona();
		try {
			persona = service.listarPersonaId(idPersona);
		}catch(Exception e) {
			return new ResponseEntity<Persona>(persona,HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<Persona>(persona,HttpStatus.OK);
	}
	
	@GetMapping(value="/listar/",produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Persona>> listarPersonas(){
		List<Persona> listaPersonas = new ArrayList<>();
		try {
			listaPersonas = service.listarPersonas();
		}catch(Exception e) {
			return new ResponseEntity<List<Persona>>(listaPersonas,HttpStatus.INTERNAL_SERVER_ERROR);
		}
			return new ResponseEntity<List<Persona>>(listaPersonas,HttpStatus.OK);
	}
	
	@DeleteMapping(value = "/eliminar/{id}",produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Integer> eliminarPersona(@PathVariable("id") Integer idPersona){
		int respuesta = 0;
		try {
			service.eliminar(idPersona);
			respuesta = 1;
		}catch(Exception e){
			return new ResponseEntity<Integer>(respuesta,HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<Integer>(respuesta,HttpStatus.OK);
	}
}
