package com.gabriel.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


import com.gabriel.model.Producto;
import com.gabriel.service.IProductoService;

@RestController
@RequestMapping("/producto")
public class ProductoController {
	@Autowired
	IProductoService service;
	

	@PostMapping(value="/registrar/",consumes=MediaType.APPLICATION_JSON_VALUE , produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Producto> registrarProducto(@RequestBody Producto productoEntrante){
		Producto producto = new Producto();
		try {
			producto = service.registrar(productoEntrante);
		}catch(Exception e) {
			return new ResponseEntity<Producto>(producto,HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<Producto>(producto,HttpStatus.OK);
	}
	
	@PutMapping(value = "/modificar/",consumes=MediaType.APPLICATION_JSON_VALUE , produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Integer> modificarProducto(@RequestBody Producto productoEntrante){
		
		int resultado = 0;
		try {
			 service.modificar(productoEntrante);
			 resultado = 1;
		}catch(Exception e) {
			return new ResponseEntity<Integer>(resultado,HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<Integer>(resultado,HttpStatus.OK);
	}
	
	@GetMapping(value = "/listar/{id}",produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Producto> listarProductoId(@PathVariable("id") Integer idProducto){
		Producto producto = new Producto();
		try {
			producto = service.listarProductoId(idProducto);
		}catch(Exception e) {
			return new ResponseEntity<Producto>(producto,HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<Producto>(producto,HttpStatus.OK);
	}
	
	@GetMapping(value="/listar/",produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Producto>> listarProductos(){
		List<Producto> listaProductos = new ArrayList<>();
		try {
			listaProductos = service.listarProductos();
		}catch(Exception e) {
			return new ResponseEntity<List<Producto>>(listaProductos,HttpStatus.INTERNAL_SERVER_ERROR);
		}
			return new ResponseEntity<List<Producto>>(listaProductos,HttpStatus.OK);
	}
	
	@DeleteMapping(value = "/eliminar/{id}",produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Integer> eliminarProducto(@PathVariable("id") Integer idProducto){
		int respuesta = 0;
		try {
			service.eliminar(idProducto);
			respuesta = 1;
		}catch(Exception e){
			return new ResponseEntity<Integer>(respuesta,HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<Integer>(respuesta,HttpStatus.OK);
	}
}
