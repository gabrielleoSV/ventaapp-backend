package com.gabriel.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


import com.gabriel.model.Venta;
import com.gabriel.service.IVentaService;

@RestController
@RequestMapping("/venta")
public class VentaController {

	@Autowired
	IVentaService service;
	
	@PostMapping(value="/registrar/",consumes=MediaType.APPLICATION_JSON_VALUE , produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Venta> registrarConsulta(@RequestBody Venta ventaEntrante){
		Venta venta = new Venta();
		try {
			venta = service.registrar(ventaEntrante);
		}catch(Exception e) {
			return new ResponseEntity<Venta>(venta,HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<Venta>(venta,HttpStatus.OK);
	}
	@GetMapping(value="/listar/",produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Venta>> listarConultas(){
		List<Venta> listaVentas = new ArrayList<>();
		try {
			listaVentas = service.listarVentas();
		}catch(Exception e) {
			return new ResponseEntity<List<Venta>>(listaVentas,HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<List<Venta>>(listaVentas,HttpStatus.OK);
	}
}
