package com.gabriel.service;

import java.util.List;

import com.gabriel.model.Persona;

public interface IPersonaService {
	Persona registrar(Persona persona);
	void modificar(Persona persona);
	void eliminar(int idPersona);
	Persona listarPersonaId(int idPersona);
	List<Persona> listarPersonas();
}
