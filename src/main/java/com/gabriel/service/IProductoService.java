package com.gabriel.service;

import java.util.List;



import com.gabriel.model.Producto;

public interface IProductoService {

	Producto registrar(Producto producto);
	void modificar(Producto producto);
	void eliminar(int idProducto);
	Producto listarProductoId(int idProducto);
	List<Producto> listarProductos();
}
