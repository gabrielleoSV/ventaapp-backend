package com.gabriel.service;

import java.util.List;



import com.gabriel.model.Venta;


public interface IVentaService {
	Venta registrar(Venta venta);
	void modificar(Venta venta);
	void eliminar(int idVenta);
	Venta listarProductoId(int idVenta);
	List<Venta> listarVentas();
}
