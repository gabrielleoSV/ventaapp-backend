package com.gabriel.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gabriel.dao.IPersonaDAO;
import com.gabriel.model.Persona;
import com.gabriel.service.IPersonaService;

@Service
public class IPersonaServiceImpl implements IPersonaService{

	@Autowired
	IPersonaDAO dao;
	@Override
	public Persona registrar(Persona persona) {
		// TODO Auto-generated method stub
		return dao.save(persona);
	}

	@Override
	public void modificar(Persona persona) {
		// TODO Auto-generated method stub
		dao.save(persona);
	}

	@Override
	public void eliminar(int idPersona) {
		// TODO Auto-generated method stub
		dao.delete(idPersona);
	}

	@Override
	public Persona listarPersonaId(int idPersona) {
		// TODO Auto-generated method stub
		return dao.findOne(idPersona);
	}

	@Override
	public List<Persona> listarPersonas() {
		// TODO Auto-generated method stub
		return dao.findAll();
	}

}
