package com.gabriel.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gabriel.dao.IProductoDAO;
import com.gabriel.model.Producto;
import com.gabriel.service.IProductoService;

@Service
public class IProductoServiceImpl implements IProductoService{

	@Autowired
	private IProductoDAO dao;
	
	@Override
	public Producto registrar(Producto producto) {
		// TODO Auto-generated method stub
		return dao.save(producto);
	}

	@Override
	public void modificar(Producto producto) {
		// TODO Auto-generated method stub
		dao.save(producto);
	}

	@Override
	public void eliminar(int idProducto) {
		// TODO Auto-generated method stub
		dao.delete(idProducto);
	}

	@Override
	public Producto listarProductoId(int idProducto) {
		// TODO Auto-generated method stub
		return dao.findOne(idProducto);
	}

	@Override
	public List<Producto> listarProductos() {
		// TODO Auto-generated method stub
		return dao.findAll();
	}

}
