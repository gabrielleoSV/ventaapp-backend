package com.gabriel.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gabriel.dao.IVentaDAO;
import com.gabriel.model.Venta;
import com.gabriel.service.IVentaService;

@Service
public class IVentaServiceImpl implements IVentaService{

	@Autowired
	IVentaDAO dao;
	@Override
	public Venta registrar(Venta venta) {
		// TODO Auto-generated method stub
		venta.getDetalleVenta().forEach(detalleVenta -> detalleVenta.setVenta(venta));
		return dao.save(venta);
	}

	@Override
	public void modificar(Venta venta) {
		// TODO Auto-generated method stub
		dao.save(venta);
	}

	@Override
	public void eliminar(int idVenta) {
		// TODO Auto-generated method stub
		dao.delete(idVenta);
	}

	@Override
	public Venta listarProductoId(int idVenta) {
		// TODO Auto-generated method stub
		return dao.findOne(idVenta);
	}

	@Override
	public List<Venta> listarVentas() {
		// TODO Auto-generated method stub
		return dao.findAll();
	}

}
